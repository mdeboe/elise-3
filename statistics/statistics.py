import json
import os
import sqlalchemy
from binance.client import Client

class Statistics():

    def __init__(self, config):
        self.client = Client(config["exchange"]["api_key"], config["exchange"]["api_secret"])
        self.metadata = sqlalchemy.MetaData()

        engine = sqlalchemy.create_engine('mysql://root:elise@192.168.99.100:3306/PROD_001')
        self.conn = engine.connect()
        create = False

        if not engine.dialect.has_table(engine, "COINS"):
            self.coins = sqlalchemy.Table("COINS", self.metadata,
                sqlalchemy.Column("ID", sqlalchemy.Integer, primary_key = True),
                sqlalchemy.Column("fullname", sqlalchemy.Text, nullable = False),
                sqlalchemy.Column("ticker", sqlalchemy.Text, nullable = False),
                sqlalchemy.Column("category", sqlalchemy.CHAR(1), nullable = False),
                sqlalchemy.Column("pair", sqlalchemy.Binary, nullable = False),
                sqlalchemy.Column("buy", sqlalchemy.Binary, nullable = False),
                sqlalchemy.Column("decimals", sqlalchemy.Integer, nullable = False),
                sqlalchemy.Column("strategy", sqlalchemy.Text, nullable = False)
            )
            create = True

        if not engine.dialect.has_table(engine, "PORTFOLIO"):
            self.portfolio = sqlalchemy.Table("PORTFOLIO", self.metadata,
                sqlalchemy.Column("startstamp", sqlalchemy.TIMESTAMP),
                sqlalchemy.Column("endstamp", sqlalchemy.TIMESTAMP),
                sqlalchemy.Column("coin", sqlalchemy.Text, nullable = False),
                sqlalchemy.Column("amount", sqlalchemy.Float),
                sqlalchemy.Column("startprice", sqlalchemy.Float),
                sqlalchemy.Column("endprice", sqlalchemy.Float),
                sqlalchemy.Column("perc", sqlalchemy.Float)
            )
            create = True

        if not engine.dialect.has_table(engine, "TRANSACTIONS"):
            self.transactions = sqlalchemy.Table("TRANSACTIONS", self.metadata,
                sqlalchemy.Column("datetimestamp", sqlalchemy.TIMESTAMP),
                sqlalchemy.Column("buycoin", sqlalchemy.Text, nullable = False),
                sqlalchemy.Column("sellcoin", sqlalchemy.Text, nullable = False),
                sqlalchemy.Column("category", sqlalchemy.CHAR(1), nullable = False),
                sqlalchemy.Column("executed", sqlalchemy.Binary, nullable = False),
                sqlalchemy.Column("stopprice", sqlalchemy.Float),
                sqlalchemy.Column("limitprice", sqlalchemy.Float),
                sqlalchemy.Column("execprice", sqlalchemy.Float),
                sqlalchemy.Column("fee", sqlalchemy.Float)
            )
            create = True

        if not engine.dialect.has_table(engine, "VALUE"):
            self.value = sqlalchemy.Table("VALUE", self.metadata,
                sqlalchemy.Column("datetimestamp", sqlalchemy.TIMESTAMP),
                sqlalchemy.Column("valuemc", sqlalchemy.Float),
                sqlalchemy.Column("valueusdt", sqlalchemy.Float)
            )
            create = True

        if create:
            self.metadata.create_all()
    


if __name__ == "__main__":
    print("started")
    filepath = os.path.realpath(__file__)

    dir_of_file = os.path.dirname(filepath)
    parent_dir_of_file = os.path.dirname(dir_of_file)
    # config = json.load(open(os.path.join(parent_dir_of_file, "config.json")))
    config = json.load(open(os.path.join(os.getcwd(), "statistics","config.json")))
    statis = Statistics(config)