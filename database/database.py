import json
import os
import logging
import sqlalchemy
from datetime import datetime

from binance_connector import Binance_Connector

logger = logging.getLogger('elise_logger')

class Database():

    def __init__(self, config):

        self.metadata = sqlalchemy.MetaData()

        if config["database"]["type"] in "sqlite3":
            engine = sqlalchemy.create_engine('sqlite://' + config["database"]["string"])
        elif config["database"]["type"] in "mysql":
            engine = sqlalchemy.create_engine('mysql://' + config["database"]["string"]) # root:elise@192.168.99.100:3306/PROD_001
        else:
            logger.critical("No Database type defined, program terminates")
            exit()

        self.conn = engine.connect()

        if not engine.dialect.has_table(engine, "EXAMPLE"):
            self.coins = sqlalchemy.Table("EXAMPLE", self.metadata,
                sqlalchemy.Column("opentime", sqlalchemy.Integer, primary_key = True),
                sqlalchemy.Column("open", sqlalchemy.FLOAT, nullable = False),
                sqlalchemy.Column("high", sqlalchemy.FLOAT, nullable = False),
                sqlalchemy.Column("low", sqlalchemy.FLOAT, nullable = False),
                sqlalchemy.Column("close", sqlalchemy.FLOAT, nullable = False),
                sqlalchemy.Column("volume", sqlalchemy.Binary, nullable = False),
                sqlalchemy.Column("quasvol", sqlalchemy.Integer, nullable = False),
                sqlalchemy.Column("takerbasevol", sqlalchemy.Text, nullable = False),
                sqlalchemy.Column("takerquotevol", sqlalchemy.Text, nullable = False)
            )
            self.metadata.create_all()

        
        if config["exchange"]["exchange"] in "binance":
            self.exchange = Binance_Connector(config["exchange"]["api_key"], config["exchange"]["api_secret"])
        else:
            logger.critical("No exchange defined, program terminates")
            exit()
    
    
    def update_coin_to_now(self, coin, last_timestamp):
        cur_dt = datetime.utcnow()
        cur_dt = cur_dt.replace(second=0, microsecond=0)
        cur_timestamp = int((cur_dt-datetime.utcfromtimestamp(0)).total_seconds())
        data, missing = self.exchange.get_data(coin.pair, last_timestamp, cur_timestamp)

        self._insert_data(coin, data)

        self._add_missing(coin, missing)
        
    
    def update_coin_between(self, coin, start, stop):
        data, missing = self.exchange.get_data(coin.pair, start, stop)

        self._insert_data(coin, data)

        self._add_missing(coin, missing)

    
    def _insert_data(self, coin, data):
        query = []

        for item in data:
            for section in item:
                for row in section:
                    insert = {}
                    insert["opentime"] = row[0]
                    insert["open"] = float(row[1])
                    insert["high"] = float(row[2])
                    insert["low"] = float(row[3])
                    insert["close"] = float(row[4])
                    insert["volume"]  = float(row[5])
                    insert["quasvol"] = float(row[7])
                    insert["takerbasevol"] = float(row[9])
                    insert["takerquotevol"] = float(row[10])
                    query.append(insert)

        self.conn.execute(coin.table.insert(), query)


    def _add_missing(self, coin, missing):
        query = []
        for miss in missing:
            before = miss[0] - 60
            query = coin.table.select().where(coin.table.c.opentime == before)
            result = self.conn.execute(query)
            row = result.fetchone()

            for t in range(miss[0], miss[1], 60):
                insert = {}
                insert["opentime"] = t
                insert["open"] = float(row[1])
                insert["high"] = float(row[2])
                insert["low"] = float(row[3])
                insert["close"] = float(row[4])
                insert["volume"]  = float(row[5])
                insert["quasvol"] = float(row[6])
                insert["takerbasevol"] = float(row[7])
                insert["takerquotevol"] = float(row[8])
                query.append(insert)

        self.conn.execute(coin.table.insert(), query)

    
    def insert_transactions(self, coin, trades = None):
        if trades is None:
            trades = self.exchange.get_trades(coin.pair)
        self.conn.execute(self.transactions.insert(), trades)


    def update_transactions(self, coin, data=None):
        query = self.transactions.update(self.metadata).where(self.transactions.c.orderid==data["orderid"].values(data))

    def get_transaction(self, orderid):
        pass
    
    def get_transactions(self, coin):
        pass
    

    def get_coin_latest_timestamp(self, coin):
        pass
    
    def get_major_coin(self):
        pass


    def get_fee_coin(self):
        pass
