import json
import os
import logging
from datetime import datetime, timedelta

from binance.client import Client
import binance.enums

logger = logging.getLogger('elise_logger')

class Binance_Connector():

    def __init__(self, api_key, api_secret):
        """The ELISE Binance connector
        param api_key: The Binance API key for the account
        param api_secret: The Binance API secret for the account
        """
        self.bin = Client(api_key, api_secret)

    
    def get_data(self, coin, start, stop, interval = binance.enums.KLINE_INTERVAL_1MINUTE):
        """Gets the data between two timestamps, with a defined interval, from Binance
        param coin: coin string (ex. NANOBTC)
        param start: timestamp for beginning of information
        param stop: timestamp for end of information
        param interval: the interval in which the klines get pulled, in binance.enums format
        """
        inter = 0
        data = []
        missing = []

        # Set interval in secs:
        if interval is binance.enums.KLINE_INTERVAL_1MINUTE:
            inter = 60

        # Binance works with 4 extra integers in the timestamp
        cur_timestamp = start * 1000
        end_timestamp = stop * 1000

        for sub_time in [28800000, 14400000, 7200000, 3600000, 2880000, 1440000, 720000, 360000, 180000, 120000, 60000]:
            while end_timestamp - cur_timestamp >= sub_time:
                new_data = self._klines(coin, cur_timestamp, cur_timestamp + sub_time, interval)
                self._pre_process(new_data, data, missing, cur_timestamp, cur_timestamp + sub_time, inter)
                cur_timestamp = cur_timestamp + sub_time

        # start_date = datetime.utcfromtimestamp(start)
        # stop_date = datetime.utcfromtimestamp(stop)
        
        # logger.debug("Coin: " + str(coin) + " updated between " + start_date.strftime("%Y-%m-%d %H:%M:%S") + "to " + stop_date.strftime("%Y-%m-%d %H:%M:%S"))
        
        return data, missing

    def get_trades(self, coin):
        """Get the trades for a specific coin from Binance account
        param coin: coin string (ex. NANOBTC)
        """
        response = {}
        response = self.bin.get_all_orders(symbol=coin, limit=20)
        return response

    
    def _klines(self, coin, start, stop, interval = binance.enums.KLINE_INTERVAL_1MINUTE):
        """Get the klines information from Binance
        param coin: coin string (ex. NANOBTC)
        param start: timestamp for beginning of information
        param stop: timestamp for end of information
        param interval: the interval in which the klines get pulled, in binance.enums format
        """
        response = {}
        trades = []
        response = self.bin.get_klines(symbol=coin, startTime=start, endTime=stop, interval=interval, limit=480)

        for trade in response:
            insert = {}
            insert["orderid"] = str(trade["orderId"])
            insert["datetimestamp"] = trade["time"]
            insert["coin"] = trade["symbol"]
            if float(trade["executedQty"]) > 0.0:
                insert["quantity"] = float(trade["executedQty"])
            else:
                insert["quantity"] = float(trade["origQty"])
            if trade["type"] in "MARKET":
                insert["category"] = "m"
            elif trade["type"] in "LIMIT":
                insert["category"] = "l"
            elif trade["type"] in "STOP_LOSS_LIMIT":
                insert["category"] = "s"
            elif trade["type"] in "TAKE_PROFIT_LIMIT":
                insert["category"] = "t"
            else:
                insert["category"] = "u"
                logger.error("Trade: Category unknown - orderid: " +  str(trade["orderId"]))

            if trade["side"] in "BUY":
                insert["buy"] = 1
            elif trade["side"] in "SELL":
                insert["buy"] = 1
            else:
                insert["buy"] = 0
                logger.error("Trade: Buy unknown - orderid: " +  str(trade["orderId"]))

            if trade["status"] in "CANCELLED":
                insert["executed"] = 0
            elif trade["status"] in "FILLED":
                insert["executed"] = 1
            else:
                insert["executed"] = 0
                logger.error("Trade: Executed unknown - orderid: " +  str(trade["orderId"]))
            
            insert["stopprice"] = insert["stopPrice"]
            insert["price"] = insert["price"]
            insert["fee"] = 0.0

            trades.append(insert)

        return response

    
    def _pre_process(self, new_data,data, missing, start, stop, inter = 60):
        """Make sure all timestamps in a range are added to the database coin table"""
        cur_time = round(start/1000)
        for row in new_data:
            new_dt = datetime.utcfromtimestamp(round(row[0]/1000))
            new_dt = new_dt.replace(second=0)
            row[0] = int((new_dt-datetime.utcfromtimestamp(0)).total_seconds())
            if cur_time != row[0]:
                missing.append([cur_time, row[0]])
            cur_time = row[0]
            row[6] = cur_time + inter - 1
            cur_time += inter
        
        if cur_time < round(stop/1000):
            missing.append([cur_time, round(stop/1000)])

        data.append(new_data)
        

if __name__ == "__main__":
    filepath = os.path.realpath(__file__)

    dir_of_file = os.path.dirname(filepath)
    parent_dir_of_file = os.path.dirname(dir_of_file)
    config = json.load(open(os.path.join(parent_dir_of_file, "config.json")))

    bincon = Binance_Connector(config["exchange"]["api_key"], config["exchange"]["api_secret"])
    data, missing = bincon.get_data("LINKBTC", 1559347200, 1559430000)

    with open("data.txt", "w") as outfile:
        json.dump(data, outfile)

    trades = bincon.get_trades("LINKBTC")
    with open("trades.txt", "w") as outfile:
        json.dump(trades, outfile)

